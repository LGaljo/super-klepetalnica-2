//Regex izraz za iskanje nizov v besedilu
var regExpSlika = /(http|https).*?\.(png|jpg|gif)/;
var regExpSmesko = /(sandbox\.lavbic\.net\/teaching\/OIS\/gradivo)/;

/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

/**
 * Zamenjava povezave do slike z sliko vstavljeno v klepet
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje povezavo do slike
 */
function dodajSliko(vhodnoBesedilo) {
  // V besedilu poiščem nize, ki označujejo sliko in smeške
  var jeSlika = regExpSlika.test(vhodnoBesedilo);
  var jeSmesko = regExpSmesko.test(vhodnoBesedilo);
  
  // vhodnoBesedilo razdelim v tabelo
  var besede = [];
  besede = vhodnoBesedilo.split(" ");
  
  // Če vhodnoBesedilo ni slika, pregledovanje izpustim
  if(!jeSlika) {
    return vhodnoBesedilo;
  } else {
    // Grem skozi tabelo in iščem link do slike, ki pa ne sme biti link do smeška
    for (var slika in besede) {
      if (regExpSlika.test(besede[slika]) && !regExpSmesko.test(besede[slika])) {
        // Vrnem vhodno besedilo in html kodo za prikaz slike
        return (vhodnoBesedilo + '<img src="' + besede[slika] + '" style="display:block; width:200px; margin-left:20px">');
      }
    }
  }
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = regExpSmesko.test(sporocilo);
  var jeSlika = regExpSlika.test(sporocilo);
  
  if (jeSmesko || jeSlika) {
    // Če je to slika ali smeško, naredim whitelisto za njihovo html kodo
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;br&gt").join("<br>")
               .split("jpg' /&gt;").join("jpg' />")
               .split("gif' /&gt;").join("gif' />")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = dodajSliko(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

/**
 * Izbrani osebi podam nadimek
 * 
 * @param uporabnik - oseba, ki ji bomo dali nadimek
 * @param nadimek
 */
var nadimki = [];
function dodajNadimek(uporabnik, nadimek) {
  var fuse = true;

  // Preverjam, če ima ta oseba že nadimek
  for (var i = 0; i < nadimki.length; i++) {
    // Če ga najdem,
    if (nadimki[i].ime == uporabnik) {
      // Prepiši stari nadimek
      nadimki[i].nadimek = nadimek;
      fuse = false;
    }
  }
  
  // Če pa uporabnik, še nima nadimka
  if(fuse) {
    // Mu ga določi
    var element = {
      ime: uporabnik,
      nadimek: nadimek,
    }
    // Dodaj element v array
    nadimki.push(element);
  }
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Počakaj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    console.log(sporocilo);
    
    // Besedilo razdelim po ':', prej je uporabniško ime, potem pa sporočilo
    var besedilo = sporocilo.besedilo.split(':');
    // Izlušči uporabniško ime
    besedilo[0] = besedilo[0].substring(0, besedilo[0].length);
    // Najdi nadimek z uporabo uporabniškega imena
    for (var i = 0; i < nadimki.length; i++) {
      // Če nima nadimka, ne določi indeksa
      if (nadimki[i].ime == besedilo[0] && nadimki[i].nadimek != null) {
        var indeksUporabnika = i;
      }
    }
    
    // Poskusi izpisati z nadimkom
    try {
      var novElement = nadimki[indeksUporabnika].nadimek + " (" + nadimki[indeksUporabnika].ime + ") " + 
      sporocilo.besedilo.substring(besedilo[0].length,sporocilo.besedilo.length);
    // Če ne rata, izpiši navadno besedilo
    } catch(err) {
      console.log(err.message);
      var novElement = sporocilo.besedilo;
    }
    // Še izpis
    $('#sporocila').append(divElementEnostavniTekst(novElement));

  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    // Izbriše celoten div
    $('#seznam-uporabnikov').empty();
    // Ponovno izpiše seznam
    for (var i=0; i < uporabniki.length; i++) {
      // Če uporabnik nima nadimka, ga ne izpiši
      var ime = uporabniki[i];
      
      // Program se obesi, če nima nadimka - to je OK
      try {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[i].nadimek + " (" + ime + ")"));
      } catch(err) {
        //console.log(err.message);
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(ime));
      }
    }

    // Klik na ime uporabnika na seznamu uporabnikov povzroči, da uporabnika krcnete
    $('#seznam-uporabnikov div').click(function() {
      klepetApp.procesirajUkaz('/krcniti ' + $(this).text());
      // Še sebi pošljem krc
      $('#sporocila').append('(Zasebno za ' + $(this).text() + '): ' + '☜');
      $('#sporocila').append('<br>');
      $('#poslji-sporocilo').focus();

    });
  });

  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
